acorn-fdisk (3.0.6-11) unstable; urgency=medium

  * QA upload.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 12.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
      - Bumped Standards-Version to 4.5.0.
      - Created VCS fields.
  * debian/copyright:
      - Added rights for Helmut Grohne.
      - Updated packaging copyright years.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/tests/control: created to perform trivial CI tests.
  * debian/upstream/metadata: created.
  * debian/watch: improved.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 15 Apr 2020 19:18:05 -0300

acorn-fdisk (3.0.6-10) unstable; urgency=low

  [ Thorsten Glaser ]
  * QA upload.
  * Apply Helmut’s patch.
  * Support the new “terse” build option, while here.
  * Update debian/copyright Format URI (lintian).

  [ Helmut Grohne ]
  * Fix FTCBFS: Don't strip during build. (Closes: #928875)

 -- Thorsten Glaser <tg@mirbsd.de>  Sun, 12 May 2019 19:29:26 +0200

acorn-fdisk (3.0.6-9) unstable; urgency=medium

  * QA upload.
  * Migrations:
      - debian/copyright to 1.0 format.
      - debian/rules to new (reduced) format.
      - DH level to 9.
  * debian/control: bumped Standards-Version to 3.9.8.
  * debian/patches/0010-add-GCC-hardening.patch: added to provide GCC hardening.
  * debian/README.source: added information about licensing.
  * debian/watch: bumped version to 4.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Wed, 24 Aug 2016 21:50:57 -0300

acorn-fdisk (3.0.6-8) unstable; urgency=low

  * QA upload
  * split diff into historic patches, change format to 3.0 (quilt)
  * lower-case short description
  * bump Standards-Version
  * modernize debian/rules:
  - switch to debhelper compatibility 7, using dh_prep
  - use dpkg-buildflags
  - support build-arch, build-indep
  * fix manpage as suggested by lintian
  * silence lintian's helper-templates-in-copyright

 -- Bernhard R. Link <brlink@debian.org>  Sat, 17 Sep 2011 16:13:51 +0200

acorn-fdisk (3.0.6-7) unstable; urgency=low

  * QA upload.
  * Acknowledge NMU (Closes: #366447) (Closes: #552791) (Closes: #563996)
    Thanks Martin!
  * Apply my patch from January 2010 (Closes: #563522)
    + fix compiler warnings:
      - fdisk.c: passing … from incompatible pointer type
      - lib/scheme/icside.c: dereferencing … does break strict-aliasing rules
        XXX there might be an endianness issue left from the original code
      - lib/scheme/pcbios.c: array subscript is above array bounds
      - lib/scheme/pcbios.c: use of uninitialised value
    + fix lintian warnings:
      - debhelper-but-no-misc-depends
      - debian-rules-ignores-make-clean-error
      - spelling-error-in-binary
      - unknown-section base
      - package-contains-empty-directory
    + debian/watch: new file (took me some time to track down upstream tho)
    + debian/control: add Homepage (as close to one as I could find)
    + debian/manpages, debian/acorn-fdisk.8: new files
  * Extend the manual page, obsoletes another bug (Closes: #436190)
  * Set maintainer to QA group. (I will however keep an eye on this
    package and, while not promising, might take it over. Not now.)
  * Add debian/source/format (preferring 1.0 at the moment)
  * Port this to GNU/kFreeBSD (some ideas from util-linux, 10x)
  * Disable building on GNU/Hurd until we have a way to port it
  * Fix more compiler warnings (format strings, unused variables)
  * Support short options in addition to GNU --long-options
  * Honour dpkg CFLAGS during build
  * debian/dirs: remove, not needed
  * debian/README.source: new, describe TODO

 -- Thorsten Glaser <tg@mirbsd.de>  Sat, 02 Oct 2010 18:45:24 +0000

acorn-fdisk (3.0.6-6.4) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control: Change Maintainer eMail address to a working
    one, as requested by Phil Blundell. (Closes: #563996)

 -- Thorsten Glaser <tg@mirbsd.de>  Fri, 22 Jan 2010 17:51:57 +0000

acorn-fdisk (3.0.6-6.3) unstable; urgency=low

  * Non-maintainer upload.
  * debian/copyright: Expand on the actual copyright owners (track
    down fdisk.c) and point to a copy of an applicable licence;
    explain which ones are applicable and why. (Closes: #552791)

 -- Thorsten Glaser <tg@mirbsd.de>  Thu, 31 Dec 2009 16:41:44 +0000

acorn-fdisk (3.0.6-6.2) unstable; urgency=low

  * NMU
  * Remove the udeb again because RiscPC machines are no longer supported
    by debian-installer.

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 14 Apr 2007 14:38:53 -0700

acorn-fdisk (3.0.6-6.1) unstable; urgency=low

  * NMU
  * Add a udeb for debian-installer, closes: #366447.
  * Move to debhelper 5.

 -- Martin Michlmayr <tbm@cyrius.com>  Fri, 12 May 2006 17:24:11 +0200

acorn-fdisk (3.0.6-6) unstable; urgency=low

  * Apply patch from Peter Naulls to fix assorted bugs:
    - Adds LBA support, and related additions to ensure fields are
      64-bit where required
    - Corrects some prompting behaviour
    - Improves some debugging with function names
    - Remove an assertion that is no longer valid under LBA, and
      therefore allow writing partitions to work again.
  Closes: #241128

 -- Philip Blundell <pb@nexus.co.uk>  Wed, 31 Mar 2004 09:45:43 +0100

acorn-fdisk (3.0.6-5) unstable; urgency=medium

  * Fix uninitialised variable in RISCiX partition reader, closes: #157697

 -- Philip Blundell <pb@debian.org>  Tue, 20 Aug 2002 23:47:04 +0100

acorn-fdisk (3.0.6-4) unstable; urgency=low

  * Avoid use of kernel headers in fdisk.c (was breaking ARM build)

 -- Philip Blundell <pb@debian.org>  Sat, 22 Dec 2001 23:21:06 +0000

acorn-fdisk (3.0.6-3) unstable; urgency=low

  * Remove "my_llseek" from blkio/write.c also; closes: #123365

 -- Philip Blundell <pb@debian.org>  Sat, 22 Dec 2001 16:21:39 +0000

acorn-fdisk (3.0.6-2) unstable; urgency=low

  * Set _FILE_OFFSET_BITS=64 for blkio library and remove "my_llseek"
    hack, closes: #123365

 -- Philip Blundell <pb@debian.org>  Sun, 16 Dec 2001 13:53:51 +0000

acorn-fdisk (3.0.6-1) unstable; urgency=low

  * Initial Release.

 -- Philip Blundell <pb@debian.org>  Sat, 10 Nov 2001 16:13:13 +0000

Local variables:
mode: debian-changelog
End:
