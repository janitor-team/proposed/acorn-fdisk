/*
 * lib/scheme/utils.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "util/debug.h"
#include "part/utils.h"

/* Function: u_int part_add (part_t *part, u_int parn, partinfo_i_t *pinfo)
 * Purpose : add a new partition (as read from disk) to the partition structures
 * Params  : part  - partitionable device
 *         : parn  - partition number
 *         : pinfo - internal partition info
 * Returns : FALSE on error
 */
u_int
part_add(part_t *part, u_int parn, partinfo_i_t *pinfo)
{
  u_int ret = 1;
  assert (part != NULL);
  assert (pinfo != NULL);

  dbg_printf("part_add(parn=%d, pinfo=[bs=0x%X, be=0x%X, type=0x%X])",
             parn, pinfo->info.blk_start, pinfo->info.blk_end, pinfo->info.type);
  dbg_level_up();

  part_updatechs(part, &pinfo->info);

  if (part->nr_partitions == 0 || parn >= part->nr_partitions) {
    partinfo_i_t **p;

    p = malloc((parn + 1) * sizeof (*p));
    if (p) {
      memset(p, 0, (parn + 1) * sizeof (*p));
      if (part->partinfo) {
        memcpy(p, part->partinfo, part->nr_partitions * sizeof (*p));
        free(part->partinfo);
      }
      part->nr_partitions = parn + 1;
      part->partinfo = p;
    } else
      ret = 0;
  }

  if (!part->partinfo[parn])
    part->partinfo[parn] = malloc(sizeof (partinfo_i_t));
  if (part->partinfo[parn]) {
    *part->partinfo[parn] = *pinfo;
    part->partinfo[parn]->changed = 0;
  } else
    ret = 0;

  dbg_level_down();
  dbg_printf("ret %s", ret ? "ok" : "failed");

  return ret;
}
