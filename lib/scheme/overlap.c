/*
 * lib/scheme/overlap.c
 *
 * Copyright (C) 1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>

#include "util/debug.h"
#include "part/utils.h"

static u_int test_overlap(const partinfo_t *p1, const partinfo_t *p2)
{
  dbg_printf("checking [0x%X-0x%X] with [0x%X-0x%X]",
             p2->blk_start, p2->blk_end,
             p1->blk_start, p1->blk_end);
  return !(p2->blk_end < p1->blk_start || p2->blk_start > p1->blk_end);
}

/* Function: u_int check_overlap(part_t *part, u_int exclude_parn, partinfo_t *pinfo)
 * Purpose : check that the new partition information `pinfo' for `exclude_parn' entry
 *           does not overlap physically on the disk with the existing partitions.
 * Params  : part         - partitionable device
 *         : exclude_parn - partition entry to exclude from check
 *         : pinfo        - new partition information to check against
 * Returns : TRUE if new information overlaps existing
 */
u_int check_overlap(part_t *part, u_int exclude_parn, const partinfo_t *pinfo)
{
  u_int i, ret = 0;

  for (i = 1; i < part->nr_partitions; i++)
    if (i != exclude_parn && part->partinfo[i] &&
        test_overlap(&part->partinfo[i]->info, pinfo)) {
      ret = 1;
      break;
    }
  return ret;
}
