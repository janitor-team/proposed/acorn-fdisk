/*
 * lib/part/chs.c
 *
 * Update CHS values when they're not valid
 */
#include <assert.h>
#include <stdlib.h>

#include "util/debug.h"
#include "util/error.h"
#include "part/part.h"

static void part_calcchs(chs_t *chs, geometry_t *geo, blk_t blk)
{
  chs->sector   = blk % geo->sectors;
  chs->head     = (blk /= geo->sectors) % geo->heads;
  chs->cylinder = blk /= geo->heads;
}

/* Prototype: u_int part_updatechs(part_t *part, partinfo_t *pinfo)
 * Purpose  : Update CHS values if they have become invalid
 * Params   : part  - partitionable device
 *          : pinfo - partition to update
 * Returns  : FALSE on error
 */
u_int part_updatechs(part_t *part, partinfo_t *pinfo)
{
  geometry_t geo;
  u_int ret = 0;

  assert(part != NULL);
  assert(pinfo != NULL);

  dbg_printf("part_updatechs([bs=0x%X, be=0x%X, type=0x%X])",
             pinfo->blk_start, pinfo->blk_end, pinfo->type);
  dbg_level_up();

  if (pinfo->chs_valid == 0) {
    if (!blkio_getgeometry(part->blkio, &geo))
      set_error("unable to get geometry");
    else {
      part_calcchs(&pinfo->chs_start, &geo, pinfo->blk_start);
      part_calcchs(&pinfo->chs_end, &geo, pinfo->blk_end);
      pinfo->chs_valid = 1;
      ret = 1;
    }
  } else
    ret = 1;

  dbg_printf("start=[C=%d,H=%d,S=%d] end=[C=%d,H=%d,S=%d]",
             pinfo->chs_start.cylinder, pinfo->chs_start.head, pinfo->chs_start.sector,
             pinfo->chs_end.cylinder, pinfo->chs_end.head, pinfo->chs_end.sector);

  dbg_level_down();
  dbg_printf("ret %s", ret ? "ok" : "error");

  return ret;
}
