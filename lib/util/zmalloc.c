/*
 * lib/util/zmalloc.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <stdlib.h>
#include <string.h>

#include "zmalloc.h"

/* Function: void *zmalloc (size_t size)
 * Purpose : malloc some memory (and clear it)
 * Params  : size - number of bytes to malloc
 * Returns : pointer to allocated memory
 */
void *zmalloc (size_t size)
{
  void *ptr = malloc (size);

  if (ptr)
    memset (ptr, 0, size);

  return ptr;
}

