/*
 * lib/util/debug.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>

static int debug_level;
static FILE *debug_file;

/* Function: void dbg_level_up (void)
 * Purpose : increase the indentation level of debugs
 */
void dbg_level_up (void)
{
  debug_level += 2;
}

/* Function: void dbg_level_up (void)
 * Purpose : decrease the indentation level of debugs
 */
void dbg_level_down (void)
{
  if (debug_level > 0)
    debug_level -= 2;
}

/* Function: void dbg_printf (const char *fmt, ...)
 * Purpose : write some debugging information
 * Params  : fmt - printf-like format string
 */
void dbg_printf (const char *fmt, ...)
{
  static char buffer[2048];
  va_list ap;

  va_start (ap, fmt);
  vsprintf (buffer, fmt, ap);
  va_end (ap);

  if (!debug_file)
    debug_file = fopen("/tmp/part.debug", "w+");

  if (debug_file)
    fprintf (debug_file, "%*s%s\n", debug_level, "", buffer);
}

/* Function: void dbg_memdump (void *data, int length)
 * Purpose : dump out a block of memory
 * Params  : data   - start of memory to dump
 *         : length - number of bytes to dump
 */
void dbg_memdump (void *data, int length)
{
  const char *byte = (char *)data;
  int offset, i;

  for (offset = 0; offset < length; offset += 16) {
  	fprintf(debug_file, "%04X : ", offset);
  	for (i = offset; i < offset + 16; i++)
  		fprintf(debug_file, "%02X ", byte[i]);
  	fprintf(debug_file, ": ");
  	for (i = offset; i < offset + 16; i++)
  		if (isprint(byte[i]))
  			fprintf(debug_file, "%c", byte[i]);
  		else
  			fprintf(debug_file, ".");
  	fprintf(debug_file, "\n");
  }
}

