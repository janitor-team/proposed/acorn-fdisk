/*
 * util/types.h
 *
 * Copyright (C) 1998 Russell King
 */
#ifndef UTIL_TYPES_H
#define UTIL_TYPES_H

#ifndef UNIX

typedef unsigned int   u_int;
typedef unsigned int   u_long;
typedef unsigned char  u_char;

#else

#include <sys/types.h>

#endif

typedef signed int     s_int;
typedef signed int     s_long;
typedef signed char    s_char;

typedef unsigned int   bool_t;

typedef unsigned int   u32;
typedef unsigned short u16;
typedef unsigned char  u8;

typedef signed int     s32;
typedef signed short   s16;
typedef signed char    s8;

#endif
