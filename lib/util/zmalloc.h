/*
 * lib/zmalloc.h
 *
 * Copyright (C) 1997,1998 Russell King
 */
#ifndef LIB_MALLOC_H
#define LIB_MALLOC_H

/* Function: void *zmalloc (size_t size)
 * Purpose : malloc some memory (and clear it)
 * Params  : size - number of bytes to malloc
 * Returns : pointer to allocated memory
 */
extern void *zmalloc (size_t size);

#endif
