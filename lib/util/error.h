/*
 * lib/error.h
 *
 * Copyright (C) 1997,1998 Russell King
 */
#ifndef ERROR_H
#define ERROR_H

#include "util/types.h"

/* Function: void set_error (const char *fmt, ...)
 * Purpose : Set error string
 * Params  : fmt - printf-like format
 */
void set_error (const char *fmt, ...);

/* Function: const char *get_error(void)
 * Purpose : Get error string
 * Returns : const pointer to error message buffer
 */
const char *get_error(void);

/* Function: u_int is_error_set(void)
 * Purpose : check to see if an error is set
 * Returns : FALSE if not set
 */
u_int is_error_set(void);

/* Function: void clear_error(void)
 * Purpose : clear error condition
 */
void clear_error(void);

#endif
