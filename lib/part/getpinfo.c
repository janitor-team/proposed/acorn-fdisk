/*
 * lib/part/getpinfo.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "util/debug.h"
#include "util/error.h"
#include "part/part.h"

/* Prototype: u_int part_getpartinfo (part_t *part, u_int parn, partinfo_t *pinfo)
 * Function : Get the partition details for partition `parn'.
 * Params   : part  - partitionable device
 *          : parn  - partition number
 *          : pinfo - partition info structure to fill out
 * Returns  : FALSE on error
 */
u_int part_getpartinfo(part_t *part, u_int parn, partinfo_t *pinfo)
{
  u_int ret = 0;

  assert(part != NULL);
  assert(pinfo != NULL);

  dbg_printf("part_getpartinfo(parn=%d)", parn);
  dbg_level_up();

  if (part->nr_partitions && parn < part->nr_partitions) {
    if (part->partinfo[parn])
      *pinfo = part->partinfo[parn]->info;
    else
      memset (pinfo, 0, sizeof (*pinfo));
    ret = 1;
  } else
    set_error("invalid partition number %d", parn);

  dbg_level_down();
  if (ret)
    dbg_printf("ret ok, pinfo=[bs=0x%X, be=0x%X, type=0x%X]",
	       pinfo->blk_start, pinfo->blk_end, pinfo->type);
  else
    dbg_printf("ret error");

  return ret;
}
