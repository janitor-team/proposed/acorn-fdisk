/*
 * lib/part/setgeo.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>

#include "util/debug.h"
#include "part/part.h"

/* Prototype: u_int part_setgeometry (part_t *part, const geometry_t *geo)
 * Function : Set the geometry of the drive
 * Params   : part - partitionable device
 *          : geo  - geometry
 * Returns  : FALSE on error
 * Notes    : This does not set the geometry that the OS' idea of the geometry.
 *          : This recalculates the CHS values for all partitions.
 */
u_int part_setgeometry(part_t *part, const geometry_t *geo)
{
  u_int ret;

  assert(part != NULL);
  assert(geo != NULL);

  dbg_printf("part_setgeometry()");
  dbg_level_up();

  ret = blkio_setgeometry(part->blkio, geo);
  if (ret) {
    u_int i;

    for (i = 0; i < part->nr_partitions; i++)
      if (part->partinfo[i]) {
        part->partinfo[i]->info.chs_valid = 0;
        part_updatechs(part, &part->partinfo[i]->info);
      }
  }

  dbg_level_down();
  dbg_printf("ret=%d", ret);

  return ret;
}
