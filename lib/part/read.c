/*
 * lib/part/read.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>

#include "util/debug.h"
#include "part/part.h"

/* Prototype: u_int part_read (part_t *part, u_int parn, void *data, blk_t blk, u_int nr_blks)
 * Function : Read blocks from a partition
 * Params   : part    - partitionable device
 *          : parn    - partition number
 *          : data    - data area to read into
 *          : blk     - starting block
 *          : nr_blks - number of blocks to read
 * Returns  : actual number of blocks read
 */
u_int part_read(part_t *part, u_int parn, void *data, blk_t blk, u_int nr_blks)
{
  partinfo_i_t *pinfo;
  u_int ret = 0;

  assert(part != NULL);
  assert(part->nr_partitions && parn < part->nr_partitions);
  assert(data != NULL);

  dbg_printf("part_read(parn=%d, data=%p, blk=0x%X, nr=0x%X)", parn, data, blk, nr_blks);
  dbg_level_up();

  pinfo = part->partinfo[parn];

  if (pinfo) {
    blk += pinfo->info.blk_start;
    if (nr_blks <= (pinfo->info.blk_end - pinfo->info.blk_start) &&
        blk < (pinfo->info.blk_end - nr_blks))
      ret = blkio_read(part->blkio, data, blk, nr_blks);
  }

  dbg_level_down();
  dbg_printf("ret=%d", ret);

  return ret;
}
