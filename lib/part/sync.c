/*
 * lib/part/sync.c
 *
 * Copyright (C) 1997,1998 Russell King
 */
#include <assert.h>
#include <stdlib.h>

#include "util/debug.h"
#include "util/error.h"
#include "part/part.h"

/* Prototype: u_int part_sync (part_t *part)
 * Function : Synchronise (write) partition information with disk.
 * Params   : part - partitionable device
 * Returns  : FALSE on error
 */
u_int part_sync (part_t *part)
{
  u_int ret = 0;
  assert (part != NULL);
  assert (part->scheme != NULL);

  dbg_printf ("part_sync()");
  dbg_level_up();

  if (part->scheme->writeinfo)
    ret = part->scheme->writeinfo(part);
  else
    set_error("partition style does not support writing of partition information");

  dbg_level_down();
  dbg_printf ("ret %sok", ret ? "" : "not ");

  return ret;
}
