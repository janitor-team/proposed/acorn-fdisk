/*
 * lib/part/schemedata.h
 *
 * Copyright (C) 1997,1998 Russell King
 */
#ifndef PART_SCHEMEDATA_H
#define PART_SCHEMEDATA_H

#include "scheme/icside.h"
#include "scheme/linux.h"
#include "scheme/pcbios.h"
#include "scheme/powertec.h"
#include "scheme/riscix.h"
#include "scheme/eesox.h"

typedef union {
#ifdef SCHEME_FILECORE_LINUX
  filecore_linux_part_t		filecore_linux;
#endif
#ifdef SCHEME_FILECORE_RISCiX
  filecore_riscix_part_t	filecore_riscix;
#endif
#ifdef SCHEME_ICSIDE
  icside_part_t			icside;
#endif
#ifdef SCHEME_PCBIOS
  pcbios_part_t			pcbios;
#endif
#ifdef SCHEME_POWERTEC
  powertec_part_t		powertec;
#endif
#ifdef SCHEME_EESOX
  eesox_part_t			eesox;
#endif
#ifdef SCHEME_FILECORE_RISCiX
  filecore_riscix_part_t	riscix;
#endif
} scheme_part_t;

typedef union {
#ifdef SCHEME_FILECORE_LINUX
  filecore_linux_data_t		filecore_linux;
#endif
#ifdef SCHEME_FILECORE_RISCiX
  filecore_riscix_data_t	filecore_riscix;
#endif
#ifdef SCHEME_ICSIDE
  icside_data_t			icside;
#endif
#ifdef SCHEME_PCBIOS
  pcbios_data_t			pcbios;
#endif
#ifdef SCHEME_POWERTEC
  powertec_data_t		powertec;
#endif
#ifdef SCHEME_EESOX
  eesox_data_t			eesox;
#endif
#ifdef SCHEME_FILECORE_RISCiX
  filecore_riscix_data_t	riscix;
#endif
} scheme_data_t;

#endif
