/*
 * lib/part/scheme.h
 *
 * Copyright (C) 1997,1998 Russell King
 */
#ifndef PART_SCHEME_H
#define PART_SCHEME_H

#include "util/types.h"

typedef struct scheme {
  /* scheme name */
  const char *name;

  /* Detect partitioning scheme */
  u_int (*detect)(part_t *);

  /* Read partition information */
  u_int (*readinfo)(part_t *);

  /* Write partition information */
  u_int (*writeinfo)(part_t *);

  /* allocate a partition number, and kernel partition number */
  u_int (*allocate)(part_t *, partinfo_t *pnew);

  /* validate an alteration to a partition */
  u_int (*validate_change)(part_t *, u_int, const partinfo_t *pold, const partinfo_t *pnew);

  /* validate creation of a partition */
  u_int (*validate_creation)(part_t *, u_int, const partinfo_t *pold, const partinfo_t *pnew);

  /* validate deletion of a partition */
  u_int (*validate_deletion)(part_t *, u_int, const partinfo_t *pold);

  /* validate a partition number */
  u_int (*validate_partno)(part_t *, u_int);

  /* return next valid partition type */
  ptyp_t (*nexttype)(part_t *, u_int, ptyp_t, int);
} scheme_t;

#endif
